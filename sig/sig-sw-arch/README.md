# sw-arch SIG

- 在openEuler社区中添加对申威架构的支持
- 实施openEuler在申威架构平台下的移植、适配和优化工作
- 负责申威架构相关软件包的规划、维护和升级


# 组织会议

- 公开的会议时间：北京时间，每双周周五下午2点~3点


# 成员
- 姚嘉伟<jiawei.yao@i-soft.com.cn> [[@georyi-yao](https://gitee.com/georyi-yao)]
- 徐想想<xiangxiang.xu@i-soft.com.cn>[[@xu-think-about-it](https://gitee.com/xu-think-about-it)]
- 阴进涛<jintao.yin@i-soft.com.cn>[[@jintao-yin](https://gitee.com/jintao-yin)]

### Maintainers列表
- 姚嘉伟[@geoyi-yao](https://gitee.com/geori-yao)
- 徐想想[@xu-think-about-it](https://gitee.com/xu-think-about-it)
- 阴进涛[@jintao-yin](https://gitee.com/jintao-yin)
